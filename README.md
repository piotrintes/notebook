# Notebook

Simple software for notes creation and managment

## Features:

- Easy notes managment
- Real-time saving (all changes are saved instantly)
- Commonly used characters symbols
- Ready-to-use styles and markers
- Shortcuts for all functions
- HTML export allowing multiple files export
- Open-file-with feature

## Install versions (recomended):

- [Linux (.deb)](release/x64/install/notebook_1.4_amd64.deb)
- [Windows](release/x64/install/notebook.exe)

## Portable versions:

- [Linux-KDE](release/x64/portable/Linux-KDE/Notebook_portable.tar.gz)
- [Windows](release/x64/portable/Windows/Notebook_portable.zip)

## Portable versions installation notes:

For non-KDE Linux distros you need to install Qt runtimes to run.<br />
If you need to compile from source I recomend using Qt Creator.

After unpacking portable version you need to add shortcuts and register file types in your system:

- *.ntkl  - Notebook library
- *.ntk   - Notebook
- *.ntks  - Notebook section

Icons for filetypes are in icons (Linux) or Icons/Filetypes (Windows)

Note: All toolbar icons delivered with Windows version are from KDE Breeze theme (I did not made them).
These icons are avalible at https://github.com/KDE/breeze-icons
