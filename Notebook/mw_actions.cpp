/****************************************************************************
 **
 ** Copyright 2019 Piotr Grosiak     Kenlin.pl Polska
 **
 ** This file is part of Notebook.
 **
 **  Notebook is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Notebook is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "notebook.h"
#include <QFile>
#include <QTextStream>
#include <QPainter>
#include <QColorDialog>
#include <QTextBlock>
#include <QTextFormat>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>


extern QVector<Notebook*> *notebookList;
extern Notebook *actNotebook;
extern QString LibFile;

void MainWindow::on_actionAlign_to_right_triggered()
{
    actEditor->setAlignment(Qt::AlignRight);
    ui->actionAlign_to_left->setChecked(false);
    ui->actionAlign_to_right->setChecked(true);
    ui->actionCenter->setChecked(false);
    ui->actionJustify->setChecked(false);
}

void MainWindow::on_actionAlign_to_left_triggered()
{
    actEditor->setAlignment(Qt::AlignLeft);
    ui->actionAlign_to_left->setChecked(true);
    ui->actionAlign_to_right->setChecked(false);
    ui->actionCenter->setChecked(false);
    ui->actionJustify->setChecked(false);
}

void MainWindow::on_actionCenter_triggered()
{
    actEditor->setAlignment(Qt::AlignCenter);
    ui->actionAlign_to_left->setChecked(false);
    ui->actionAlign_to_right->setChecked(false);
    ui->actionCenter->setChecked(true);
    ui->actionJustify->setChecked(false);
}

void MainWindow::on_actionJustify_triggered()
{
    actEditor->setAlignment(Qt::AlignJustify);
    ui->actionAlign_to_left->setChecked(false);
    ui->actionAlign_to_right->setChecked(false);
    ui->actionCenter->setChecked(false);
    ui->actionJustify->setChecked(true);
}

void MainWindow::on_actionUndo_triggered()
{
    actEditor->undo();
}

void MainWindow::on_actionRedo_triggered()
{
    actEditor->redo();
}

void MainWindow::on_actionCopy_triggered()
{
    actEditor->copy();
}

void MainWindow::on_actionCut_triggered()
{
    actEditor->cut();
}

void MainWindow::on_actionPaste_triggered()
{
    if(actEditor->canPaste())
        actEditor->paste();
}

void MainWindow::on_actionIncrease_font_size_triggered()
{
    qreal fontsize = actEditor->fontPointSize();
    if(fontsize <= 0) fontsize = 14;
    actEditor->setFontPointSize(fontsize+2);
}

void MainWindow::on_actionDecrease_font_size_triggered()
{
    qreal fontsize = actEditor->fontPointSize();
    if(fontsize <= 0) fontsize = 14;
    actEditor->setFontPointSize(fontsize-2);
}

void MainWindow::on_actionMarker1_triggered(bool checked)
{
    if(checked)
        actEditor->setTextBackgroundColor(QColor(100,255,150));
    else
        actEditor->setTextBackgroundColor(QColor(255,255,255,0));
}

void MainWindow::on_actionMarker2_triggered(bool checked)
{
    if(checked)
        actEditor->setTextBackgroundColor(QColor(255,255,150));
    else
        actEditor->setTextBackgroundColor(QColor(255,255,255,0));
}

void MainWindow::on_actionMarker3_triggered(bool checked)
{
    if(checked)
        actEditor->setTextBackgroundColor(QColor(255,150,150));
    else
        actEditor->setTextBackgroundColor(QColor(255,255,255,0));
}

void MainWindow::on_actionCustom_marker_triggered(bool checked)
{
    if(checked)
        actEditor->setTextBackgroundColor(customMarkerColor);
    else
        actEditor->setTextBackgroundColor(QColor(255,255,255,0));
}

void MainWindow::on_actionSet_custom_marker_color_triggered()
{
    //ask for user color
    QColor color = QColorDialog::getColor(actEditor->textBackgroundColor(), this,"Set custom marker color");
    if(color != QColor(0,0,0)) customMarkerColor = color;
    //update icon
    QPixmap px(16,16);
    px.fill(QColor(0,0,0,0));
    QPainter painter(&px);
    painter.fillRect(0,0,16,16,customMarkerColor);
    painter.drawText(1,14,"Tt");
    QIcon icon(px);
    ui->actionCustom_marker->setIcon(icon);
}

void MainWindow::on_actionSelect_all_triggered()
{
    actEditor->selectAll();
}

void MainWindow::on_actionOpen_triggered()
{
    QFileInfo file(QFileDialog::getOpenFileName(this,
        "Open notebook", "",
        "All Notebook file types (*.ntk *.ntkl *.ntks)\n"
        "Notebook (*.ntk)\n"
        "Notebook library (*.ntkl)\n"
        "Notebook section (*.ntks)\n"
        "HTML files (*.htm *.html)\n"
        "Plain text files (*.txt)\n"
        "All supported file types (*.ntk *.ntks *.ntkl *.txt *.htm *.html)"));

    if(!file.exists()) return;

    openFile(file);
}

void MainWindow::on_actionQuit_triggered()
{
    QApplication::exit(0);
}

void MainWindow::on_actionNewSection_triggered()
{
    if(actNotebook == nullptr) return;
    QString name = QInputDialog::getText(this,"New section","Input section name:");
    if (name != "")
    {
        actNotebook->addSection(name);
        ui->sectionList->setCurrentRow(ui->sectionList->count()-1);
    }
}

void MainWindow::on_actionNewNotebook_triggered()
{
    QFileInfo file(QFileDialog::getSaveFileName(this, "Save notebook", "", "Notebook (*.ntk)"));
    if (file.path() == "") return;
    QColor color = QColorDialog::getColor(QColor(0,0,0), this, "Set notebook color");

    if(file.suffix() != "ntk") file.setFile(file.absoluteFilePath() + ".ntk");

    Notebook *notebook = new Notebook(this,file.path() + "/", file.fileName().left(file.fileName().length()-file.suffix().length()-1));
    notebook->setColor(color);
    notebook->select();
}

void MainWindow::on_actionSave_as_triggered()
{
    QFileInfo file(QFileDialog::getSaveFileName(this, "Save section", "", "Notebook section (*.ntks)"));
    if (file.path() == "") return;
    saveNotebookSection(file.absoluteFilePath());
}

void MainWindow::on_actionSave_all_notebooks_as_triggered()
{
    QFileInfo filep(QFileDialog::getSaveFileName(this, "Save library", "", "Notebook library (*.ntkl)"));
    if (filep.path() == "") return;

    if(filep.suffix() != "ntkl") filep.setFile(filep.absoluteFilePath() + ".ntkl");

    QFile file(filep.absoluteFilePath());
    file.remove();
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        for(int i=0;i<notebookList->count();i++)
            stream << notebookList->at(i)->path + notebookList->at(i)->title + "\n";
    }
    file.close();
    LibFile = filep.absoluteFilePath();
}

void MainWindow::on_actionExport_to_HTML_triggered()
{
    QFileInfo filei(QFileDialog::getSaveFileName(this, "Export section", "", "HTML file (*.html)\nHTM file (*.htm)"));
    if (filei.path() == "") return;

    QFile file(filei.absoluteFilePath());
    file.remove();
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        stream << ui->textEdit->toHtml();
    }
    file.close();
}

void MainWindow::on_actionStyle_normal_text_triggered()
{
    actEditor->setFontFamily(this->font().family());
    actEditor->setFontItalic(false);
    actEditor->setFontUnderline(false);
    actEditor->setFontWeight(QFont::Normal);
    actEditor->setFontPointSize(this->font().pointSize()+2);
    actEditor->setAlignment(Qt::AlignLeft);
}

void MainWindow::on_actionStyle_Header_1_triggered()
{
    actEditor->setFontFamily(this->font().family());
    actEditor->setFontItalic(false);
    actEditor->setFontUnderline(false);
    actEditor->setFontWeight(QFont::Bold);
    actEditor->setFontPointSize(this->font().pointSize()+8);
    actEditor->setAlignment(Qt::AlignCenter);
}

void MainWindow::on_actionStyle_Header_2_triggered()
{
    actEditor->setFontFamily(this->font().family());
    actEditor->setFontItalic(false);
    actEditor->setFontUnderline(false);
    actEditor->setFontWeight(QFont::Bold);
    actEditor->setFontPointSize(this->font().pointSize()+8);
    actEditor->setAlignment(Qt::AlignLeft);
}

void MainWindow::on_actionStyle_Header_3_triggered()
{
    actEditor->setFontFamily(this->font().family());
    actEditor->setFontItalic(false);
    actEditor->setFontUnderline(false);
    actEditor->setFontWeight(QFont::Bold);
    actEditor->setFontPointSize(this->font().pointSize()+4);
    actEditor->setAlignment(Qt::AlignLeft);
}

void MainWindow::on_actionExport_Notebook_triggered()
{
    QFileInfo filei(QFileDialog::getSaveFileName(this, "Export Notebook", "", "HTML file (*.html)\nHTM file (*.htm)"));
    if (filei.path() == "") return;
    actNotebook->exportNotebook(filei.absoluteDir(),filei.fileName(),filei.suffix());
}

void MainWindow::on_actionExport_Library_triggered()
{
    QFileInfo filei(QFileDialog::getSaveFileName(this, "Export Library", "", "HTML file (*.html)\nHTM file (*.htm)"));
    if (filei.path() == "") return;

    QDir dir = filei.absoluteDir();
    QString name = filei.fileName().left(filei.fileName().length() - filei.suffix().length()-1);

    QDir d;
    d.mkdir(dir.absolutePath() + "/" + name + "/");


    for (int i=0;i<notebookList->count();i++)
    {
        QDir d(dir.absolutePath() + "/" + notebookList->at(i)->title);
        d.mkdir(dir.absolutePath() + "/" + notebookList->at(i)->title);
        notebookList->at(i)->exportNotebook(d, "index." + filei.suffix(),filei.suffix());
    }


    QFile file(dir.absolutePath() + "/" + name + "/list." + filei.suffix());
    file.remove();
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        stream << "<style>\n" <<
                  "body{\n" <<
                  "  background-color: #00a81f;\n" <<
                  "  margin-left: 0px;\n" <<
                  "}\n" <<
                  "h3{\n" <<
                  "  color: white;\n" <<
                  "  padding: 10px 0px 10px 20px;\n" <<
                  "}\n" <<
                  "a:link, a:visited {\n" <<
                  "  background-color: #00a81f;\n" <<
                  "  color: white;\n" <<
                  "  font-weight: bold;\n" <<
                  "  padding: 10px 0px 10px 20px;\n" <<
                  "  text-decoration: none;\n" <<
                  "  display: inline-block;\n" <<
                  "  width: 90%;\n" <<
                  "  -webkit-transition: color 0.5s, background-color 0.5s;\n" <<
                  "  transition: color 0.5s, background-color 0.5s;\n" <<
                  "}\n" <<
                  "a:hover, a:active, a:focus {\n" <<
                  "  background-color: #188f27;\n" <<
                  "  -webkit-transition: color 0.5s, background-color 0.5s;\n" <<
                  "  transition: color 0.5s, background-color 0.5s;\n" <<
                  "  border: 0" <<
                  "}\n" <<
                  "</style>\n" <<
                  "<h3>Notebook:</h3>\n";
        for(int i=0;i<notebookList->count();i++)
        {
            QString notebookFile = notebookList->at(i)->title + "/index." + filei.suffix();
            stream << "<a href=\"../" << notebookFile << "\" target=main1>" << notebookList->at(i)->title << "</a><br>"  ;
        }
    }
    file.close();

    QFile indexFile(dir.absolutePath() + "/" + filei.fileName());
    indexFile.remove();
    if ( indexFile.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &indexFile );
        stream << "<frameset cols=\"200,*\" border=0>";
        stream << "<frame src=\"" + name + "/list." + filei.suffix() + "\">";
        stream << "<frame name=main1 src=\"" + notebookList->at(0)->title + "/" + notebookList->at(0)->title + "/notebook.htm\">";
        stream << "</frameset>";
    }
    indexFile.close();
}

void MainWindow::on_action_alpha_triggered()
{
    actEditor->insertPlainText("α");
}

void MainWindow::on_action_beta_triggered()
{
    actEditor->insertPlainText("β");
}

void MainWindow::on_action_gamma_triggered()
{
    actEditor->insertPlainText("γ");
}

void MainWindow::on_action_lambda_triggered()
{
    actEditor->insertPlainText("λ");
}

void MainWindow::on_action_delta_triggered()
{
    actEditor->insertPlainText("Δ");
}

void MainWindow::on_action_pi_triggered()
{
    actEditor->insertPlainText("π");
}

void MainWindow::on_action_omegaS_triggered()
{
    actEditor->insertPlainText("ω");
}

void MainWindow::on_action_epsilon_triggered()
{
    actEditor->insertPlainText("Σ");
}

void MainWindow::on_action_omega_triggered()
{
    actEditor->insertPlainText("Ω");
}

void MainWindow::on_action_dot_triggered()
{
    actEditor->insertPlainText(ui->action_dot->text());
}

void MainWindow::on_action_minus_triggered()
{
    actEditor->insertPlainText(ui->action_minus->text());
}

void MainWindow::on_action_neq_triggered()
{
    actEditor->insertPlainText(ui->action_neq->text());
}

void MainWindow::on_action_paragraph_triggered()
{
    actEditor->insertPlainText(ui->action_paragraph->text());
}

void MainWindow::on_action_lessOrEq_triggered()
{
    actEditor->insertPlainText(ui->action_lessOrEq->text());
}

void MainWindow::on_action_moreOrEq_triggered()
{
    actEditor->insertPlainText(ui->action_moreOrEq->text());
}

void MainWindow::on_action_promil_triggered()
{
    actEditor->insertPlainText(ui->action_promil->text());
}

void MainWindow::on_action_cross_triggered()
{
    actEditor->insertPlainText(ui->action_cross->text());
}

void MainWindow::on_action_div_triggered()
{
    actEditor->insertPlainText(ui->action_div->text());
}

void MainWindow::on_action_similar_triggered()
{
    actEditor->insertPlainText(ui->action_similar->text());
}

void MainWindow::on_action_plusminus_triggered()
{
    actEditor->insertPlainText(ui->action_plusminus->text());
}

void MainWindow::on_action_dotOpen_triggered()
{
    actEditor->insertPlainText(ui->action_dotOpen->text());
}

void MainWindow::on_action_inf_triggered()
{
    actEditor->insertPlainText(ui->action_inf->text());
}

void MainWindow::on_action_arrow_lr_triggered()
{
    actEditor->insertPlainText(ui->action_arrow_lr->text());
}

void MainWindow::on_action_arrow_l_triggered()
{
    actEditor->insertPlainText(ui->action_arrow_l->text());
}

void MainWindow::on_action_arrow_u_triggered()
{
    actEditor->insertPlainText(ui->action_arrow_u->text());
}

void MainWindow::on_action_arrow_d_triggered()
{
    actEditor->insertPlainText(ui->action_arrow_d->text());
}

void MainWindow::on_action_arrow_r_triggered()
{
    actEditor->insertPlainText(ui->action_arrow_r->text());
}

void MainWindow::on_action_c_triggered()
{
    actEditor->insertPlainText(ui->action_c->text());
}

void MainWindow::on_action_r_triggered()
{
    actEditor->insertPlainText(ui->action_r->text());
}

void MainWindow::on_action_euro_triggered()
{
    actEditor->insertPlainText(ui->action_euro->text());
}

void MainWindow::on_action_pe_triggered()
{
    actEditor->insertPlainText(ui->action_pe->text());
}

void MainWindow::on_actionRename_section_triggered()
{
    if(actNotebook == nullptr) return;
    QString name = QInputDialog::getText(this,"Rename section","Input section name:",QLineEdit::Normal,ui->sectionList->currentItem()->text());
    if (name != "")
    {
        QString firstFilename = actFilename;
        actNotebook->renameSection(ui->sectionList->currentItem()->text(),name);
        ui->sectionList->currentItem()->setText(name);
        actFilename = ui->sectionList->currentItem()->data(-1).toString() + "/" + name;
        saveNotebookSection(actFilename);
        QFile::remove(firstFilename + ".ntks");
    }
}

void MainWindow::on_actionRename_triggered()
{
    if(actNotebook == nullptr) return;
    QString name = QInputDialog::getText(this,"Rename notebook","Input notebook name:",QLineEdit::Normal,actNotebook->title);
    if (name != "")
    {
        actNotebook->renameNotebook(name);
        if(LibFile != "")
        {
            QFile file(LibFile);
            file.remove();
            if ( file.open(QIODevice::ReadWrite) )
            {
                QTextStream stream( &file );
                for(int i=0;i<notebookList->count();i++)
                    stream << notebookList->at(i)->path + notebookList->at(i)->title + "\n";
            }
            file.close();
        }
    }
}

void MainWindow::on_actionEdit_color_triggered()
{
    if(actNotebook == nullptr) return;
    QColor color = QColorDialog::getColor(actNotebook->color, this, "Change notebook color");
    actNotebook->setColor(color);
}


void MainWindow::on_actionGreek_toggled(bool arg1)
{
    ui->action_alpha->setVisible(arg1);
    ui->action_beta->setVisible(arg1);
    ui->action_gamma->setVisible(arg1);
    ui->action_lambda->setVisible(arg1);
    ui->action_pi->setVisible(arg1);
    ui->action_omegaS->setVisible(arg1);
    ui->action_delta->setVisible(arg1);
    ui->action_omega->setVisible(arg1);
    ui->action_epsilon->setVisible(arg1);
}

void MainWindow::on_actionMathematical_toggled(bool arg1)
{
    ui->action_dot->setVisible(arg1);
    ui->action_div->setVisible(arg1);
    ui->action_minus->setVisible(arg1);
    ui->action_plusminus->setVisible(arg1);
    ui->action_dotOpen->setVisible(arg1);
    ui->action_cross->setVisible(arg1);
}

void MainWindow::on_actionComparators_toggled(bool arg1)
{
    ui->action_lessOrEq->setVisible(arg1);
    ui->action_moreOrEq->setVisible(arg1);
    ui->action_neq->setVisible(arg1);
    ui->action_similar->setVisible(arg1);
}

void MainWindow::on_actionArrows_toggled(bool arg1)
{
    ui->action_arrow_l->setVisible(arg1);
    ui->action_arrow_r->setVisible(arg1);
    ui->action_arrow_d->setVisible(arg1);
    ui->action_arrow_u->setVisible(arg1);
    ui->action_arrow_lr->setVisible(arg1);
}

void MainWindow::on_actionSymbols_toggled(bool arg1)
{
    ui->action_promil->setVisible(arg1);
    ui->action_inf->setVisible(arg1);
    ui->action_paragraph->setVisible(arg1);
    ui->action_c->setVisible(arg1);
    ui->action_r->setVisible(arg1);
    ui->action_euro->setVisible(arg1);
    ui->action_pe->setVisible(arg1);
}
