/****************************************************************************
 **
 ** Copyright 2019 Piotr Grosiak     Kenlin.pl Polska
 **
 ** This file is part of Notebook.
 **
 **  Notebook is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Notebook is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef SECTION_H
#define SECTION_H
#include <QObject>
#include <QListWidgetItem>

class Section : public QObject
{
public:
    QString path;                                           // Path to section file
    QString title;                                          // Section filename displayed on list
    QListWidgetItem *listItem = new QListWidgetItem();      // List item representing section
    explicit Section(QString path, QString name);           // Section constructor
signals:

public slots:
};

#endif // SECTION_H
