/****************************************************************************
 **
 ** Copyright 2019 Piotr Grosiak     Kenlin.pl Polska
 **
 ** This file is part of Notebook.
 **
 **  Notebook is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Notebook is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "mytoolbar.h"
#include <QPainter>

MyToolBar::MyToolBar(QWidget *parent): QToolBar(parent)
{
    bcgColor = palette().background().color();
    QPalette pal = palette();
    pal.setColor(QPalette::Window,QColor(0,0,0,0));
    setPalette(pal);
}

void MyToolBar::setColorsVector(QVector<QColor> *colors)
{
    delete this->colors;

    QVector<QColor> *colors1 = new QVector<QColor>;

    int blurEx = 5;
    int precision = 50;

    colors1->append(colors->at(0));
    for(int i=50;i<width()-50*2;i=i+50)
    {
        //mix colors form several pixels around selected point
        int r=0;
        int g=0;
        int b=0;
        for(int p=1;p<blurEx;p++)
        {
            r += QColor(colors->at(i-(precision/blurEx)*p)).red();
            g += QColor(colors->at(i-(precision/blurEx)*p)).green();
            b += QColor(colors->at(i-(precision/blurEx)*p)).blue();
            r += QColor(colors->at(i+(precision/blurEx)*p)).red();
            g += QColor(colors->at(i+(precision/blurEx)*p)).green();
            b += QColor(colors->at(i+(precision/blurEx)*p)).blue();
        }
        r += QColor(colors->at(i)).red();
        g += QColor(colors->at(i)).green();
        b += QColor(colors->at(i)).blue();

        colors1->append(QColor(r/(blurEx*2+1),g/(blurEx*2+1),b/(blurEx*2+1)));
    }
    //add last point mixed with left only
    int r=0;
    int g=0;
    int b=0;
    for(int p=1;p<blurEx;p++)
    {
        r += QColor(colors->at(width()-precision-(precision/blurEx)*p)).red();
        g += QColor(colors->at(width()-precision-(precision/blurEx)*p)).green();
        b += QColor(colors->at(width()-precision-(precision/blurEx)*p)).blue();
    }
    r += QColor(colors->at(width()-precision)).red();
    g += QColor(colors->at(width()-precision)).green();
    b += QColor(colors->at(width()-precision)).blue();
    colors1->append(QColor(r/(blurEx+1),g/(blurEx+1),b/(blurEx+1)));


    delete colors;
    this->colors = colors1;
    repaint();
}

void MyToolBar::setOffest(int offest)
{
    this->offest = offest;
    repaint();
}

void MyToolBar::setMode(int mode)
{
    this->mode = mode;
    repaint();
}

void MyToolBar::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    if(colors != nullptr)
    {
        if(mode < 2)
        {
            QRect rect(offest,0,width()-offest,height());
            QLinearGradient gradient(offest,0, width()+offest,0);

            //set blur gradient
            for(int i=0;i<colors->count();i++)
            {
                double j = i;
                gradient.setColorAt(j/(colors->count()), colors->at(i));
            }
            //draw blur effect
            painter.fillRect(rect, gradient);

            //draw left horizontal gradient (blend blur with frame color)
            if(offest > 0)
            {
                QRect rect1(offest,0,100,height());
                QLinearGradient gradient1(offest,0,offest+100,0);
                gradient1.setColorAt(0,bcgColor);
                gradient1.setColorAt(1,QColor(0,0,0,0));
                painter.fillRect(rect1, gradient1);
            }

            //draw right horizontal gradient (blend blur with frame color)
            QRect rect1(width()-50,0,50,height());
            QLinearGradient gradient1(width()-50,0,width(),0);
            gradient1.setColorAt(0,QColor(0,0,0,0));
            gradient1.setColorAt(1,bcgColor);
            painter.fillRect(rect1, gradient1);

            //draw vertical gradient (blend blur with frame color)
            if(mode)
            {
                QLinearGradient gradient2(0,-height()*3,0,height());
                gradient2.setColorAt(0,QColor(0,0,0,0));
                gradient2.setColorAt(1,bcgColor);
                painter.fillRect(rect, gradient2);
            } else {
                QLinearGradient gradient2(0,0,0,height()*4);
                gradient2.setColorAt(0,bcgColor);
                gradient2.setColorAt(1,QColor(0,0,0,0));
                painter.fillRect(rect, gradient2);
            }
        }
    }

    QToolBar::paintEvent(event);     // this will draw the base class content
}


