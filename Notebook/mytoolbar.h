/****************************************************************************
 **
 ** Copyright 2019 Piotr Grosiak     Kenlin.pl Polska
 **
 ** This file is part of Notebook.
 **
 **  Notebook is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Notebook is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef MYTOOLBAR_H
#define MYTOOLBAR_H
#include <QToolBar>

class MyToolBar : public QToolBar
{
    Q_OBJECT
private:
    QVector<QColor> *colors = nullptr;                          //Vector of colors used to create gradient
    int offest = 0;                                             //Offest from left edge to gradient's beginning
    int mode = 0;                                               /* 0 - top bar
                                                                   1 - bottom bar */
    QColor bcgColor = QColor(0,0,0,0);                          //background color
public:
    MyToolBar(QWidget *parent = nullptr);
    void setColorsVector(QVector<QColor> *colors);              //Set colors vector
    void setOffest(int offest);                                 //Set offest variable
    void setMode(int mode);                                     //Set mode variable

protected:
    virtual void paintEvent(QPaintEvent *event);                //Overloaded paint event

};

#endif // MYTOOLBAR_H
