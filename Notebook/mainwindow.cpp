/****************************************************************************
 **
 ** Copyright 2019 Piotr Grosiak     Kenlin.pl Polska
 **
 ** This file is part of Notebook.
 **
 **  Notebook is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Notebook is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "notebook.h"
#include <QFile>
#include <QTextStream>
#include <QPainter>
#include <QColorDialog>
#include <QTextBlock>
#include <QTextFormat>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>

extern QVector<Notebook*> *notebookList;                    // List of pointers to all opened notebooks (required for library managment)
QVector<Notebook*> *notebookList = new QVector<Notebook*>;

extern Notebook *actNotebook;                               // Actually selected notebook. Nullptr = Opened file is not section of any notebook
Notebook *actNotebook = nullptr;

extern QString LibFile;                                     // Actual Library file. "" = No library is being used
QString LibFile = "";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->actionAdd_comment->setVisible(false);

    ui->hideLeftBar->setOrientation(OrientablePushButton::VerticalBottomToTop);

    on_hideLeftBar_clicked();

    //setup default markers icons
    QPixmap px(16,16);
    px.fill(QColor(0,0,0,0));
    QPainter painter(&px);
    painter.setPen(palette().windowText().color());
    //Markers icons
    {
        painter.drawText(1,14,"Tt");
        QIcon icon(px);
        ui->actionCustom_marker->setIcon(icon);
    }
    {
        painter.fillRect(0,0,16,16,QColor(100,255,150));
        painter.fillRect(0,15,16,16,QColor(0,200,50));
        painter.drawText(1,14,"Tt");
        QIcon icon(px);
        ui->actionMarker1->setIcon(icon);
    }
    {
        painter.fillRect(0,0,16,16,QColor(255,255,150));
        painter.fillRect(0,15,16,16,QColor(200,200,50));
        painter.drawText(1,14,"Tt");
        QIcon icon(px);
        ui->actionMarker2->setIcon(icon);
    }
    {
        painter.fillRect(0,0,16,16,QColor(255,150,150));
        painter.fillRect(0,15,16,16,QColor(200,50,50));
        painter.drawText(1,14,"Tt");
        QIcon icon(px);
        ui->actionMarker3->setIcon(icon);
    }
    //Styles icons
    {
        px.fill(QColor(0,0,0,0));
        painter.drawText(-1,14,"Nt");
        QIcon icon(px);
        ui->actionStyle_normal_text->setIcon(icon);
    }
    QPixmap px2(22,22);
    px2.fill(QColor(0,0,0,0));
    QPainter painter2(&px2);
    painter2.setPen(palette().windowText().color());
    QFont f = painter.font();
    f.setBold(true);
    painter2.setFont(f);
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(0,17,"H1");
        QIcon icon(px2);
        ui->actionStyle_Header_1->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(0,17,"H2");
        QIcon icon(px2);
        ui->actionStyle_Header_2->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(0,17,"H3");
        QIcon icon(px2);
        ui->actionStyle_Header_3->setIcon(icon);
    }
    //Special characters icons
    f.setBold(false);
    f.setPointSize(f.pointSize()+4);
    painter2.setFont(f);
    //Greek
    ui->action_alpha->setVisible(false);
    ui->action_beta->setVisible(false);
    ui->action_gamma->setVisible(false);
    ui->action_lambda->setVisible(false);
    ui->action_pi->setVisible(false);
    ui->action_omegaS->setVisible(false);
    ui->action_delta->setVisible(false);
    ui->action_omega->setVisible(false);
    ui->action_epsilon->setVisible(false);
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(3,20,"α");
        QIcon icon(px2);
        ui->action_alpha->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(3,20,"β");
        QIcon icon(px2);
        ui->action_beta->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(3,18,"γ");
        QIcon icon(px2);
        ui->action_gamma->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(3,20,"λ");
        QIcon icon(px2);
        ui->action_lambda->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(3,20,"π");
        QIcon icon(px2);
        ui->action_pi->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(3,20,"ω");
        QIcon icon(px2);
        ui->action_omegaS->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(3,20,"Δ");
        QIcon icon(px2);
        ui->action_delta->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(3,20,"Ω");
        QIcon icon(px2);
        ui->action_omega->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(3,20,"Σ");
        QIcon icon(px2);
        ui->action_epsilon->setIcon(icon);
    }
    //Mathematical
    ui->action_dot->setVisible(false);
    ui->action_div->setVisible(false);
    ui->action_minus->setVisible(false);
    ui->action_plusminus->setVisible(false);
    ui->action_dotOpen->setVisible(false);
    ui->action_cross->setVisible(false);
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(6,20,"·");
        QIcon icon(px2);
        ui->action_dot->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(6,20,"÷");
        QIcon icon(px2);
        ui->action_div->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(6,20,"–");
        QIcon icon(px2);
        ui->action_minus->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(6,20,"±");
        QIcon icon(px2);
        ui->action_plusminus->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(6,20,"°");
        QIcon icon(px2);
        ui->action_dotOpen->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(6,20,"×");
        QIcon icon(px2);
        ui->action_cross->setIcon(icon);
    }
    //Comparations
    ui->action_lessOrEq->setVisible(false);
    ui->action_moreOrEq->setVisible(false);
    ui->action_neq->setVisible(false);
    ui->action_similar->setVisible(false);
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"≤");
        QIcon icon(px2);
        ui->action_lessOrEq->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"≥");
        QIcon icon(px2);
        ui->action_moreOrEq->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"≠");
        QIcon icon(px2);
        ui->action_neq->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"≈");
        QIcon icon(px2);
        ui->action_similar->setIcon(icon);
    }
    //Arrows
    ui->action_arrow_l->setVisible(false);
    ui->action_arrow_r->setVisible(false);
    ui->action_arrow_d->setVisible(false);
    ui->action_arrow_u->setVisible(false);
    ui->action_arrow_lr->setVisible(false);
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"←");
        QIcon icon(px2);
        ui->action_arrow_l->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"→");
        QIcon icon(px2);
        ui->action_arrow_r->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"↓");
        QIcon icon(px2);
        ui->action_arrow_d->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"↑");
        QIcon icon(px2);
        ui->action_arrow_u->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"↔");
        QIcon icon(px2);
        ui->action_arrow_lr->setIcon(icon);
    }
    //Symbols
    ui->action_promil->setVisible(false);
    ui->action_inf->setVisible(false);
    ui->action_paragraph->setVisible(false);
    ui->action_c->setVisible(false);
    ui->action_r->setVisible(false);
    ui->action_euro->setVisible(false);
    ui->action_pe->setVisible(false);
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(-1,20,"‰");
        QIcon icon(px2);
        ui->action_promil->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"∞");
        QIcon icon(px2);
        ui->action_inf->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(5,20,"§");
        QIcon icon(px2);
        ui->action_paragraph->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"©");
        QIcon icon(px2);
        ui->action_c->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(2,20,"®");
        QIcon icon(px2);
        ui->action_r->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(5,20,"€");
        QIcon icon(px2);
        ui->action_euro->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(5,20,"£");
        QIcon icon(px2);
        ui->action_pe->setIcon(icon);
    }
    //Symbols category icons
    f.setPointSize(f.pointSize()-4);
    painter2.setFont(f);
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(0,10,"α");
        painter2.drawText(9,22,"Ω");
        QIcon icon(px2);
        ui->actionGreek->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(0,10,"÷");
        painter2.drawText(9,22,"±");
        QIcon icon(px2);
        ui->actionMathematical->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(0,10,"≈");
        painter2.drawText(9,22,"≤");
        QIcon icon(px2);
        ui->actionComparators->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(-2,15,"↑");
        painter2.drawText(7,22,"↔");
        QIcon icon(px2);
        ui->actionArrows->setIcon(icon);
    }
    {
        px2.fill(QColor(0,0,0,0));
        painter2.drawText(0,15,"§");
        painter2.drawText(9,22,"©");
        QIcon icon(px2);
        ui->actionSymbols->setIcon(icon);
    }

    //Other icons (skip on linux)
    #ifndef Q_OS_LINUX
        this->setWindowIcon(QIcon("Icons/icon.png"));
        ui->actionBold->setIcon(QIcon(QPixmap("Icons/Toolbar/format-text-bold.svg")));
        ui->actionItalic->setIcon(QIcon(QPixmap("Icons/Toolbar/format-text-italic.svg")));
        ui->actionUnderline->setIcon(QIcon(QPixmap("Icons/Toolbar/format-text-underline.svg")));
        ui->actionAlign_to_left->setIcon(QIcon(QPixmap("Icons/Toolbar/format-justify-left.svg")));
        ui->actionAlign_to_right->setIcon(QIcon(QPixmap("Icons/Toolbar/format-justify-right.svg")));
        ui->actionCenter->setIcon(QIcon(QPixmap("Icons/Toolbar/format-justify-center.svg")));
        ui->actionJustify->setIcon(QIcon(QPixmap("Icons/Toolbar/format-justify-fill.svg")));
        ui->actionCut->setIcon(QIcon(QPixmap("Icons/Toolbar/edit-cut.svg")));
        ui->actionCopy->setIcon(QIcon(QPixmap("Icons/Toolbar/edit-copy.svg")));
        ui->actionPaste->setIcon(QIcon(QPixmap("Icons/Toolbar/edit-paste.svg")));
        ui->actionUndo->setIcon(QIcon(QPixmap("Icons/Toolbar/edit-undo.svg")));
        ui->actionRedo->setIcon(QIcon(QPixmap("Icons/Toolbar/edit-redo.svg")));
        ui->actionSelect_all->setIcon(QIcon(QPixmap("Icons/Toolbar/edit-select-all.svg")));
        ui->actionIncrease_font_size->setIcon(QIcon(QPixmap("Icons/Toolbar/format-font-size-more.svg")));
        ui->actionDecrease_font_size->setIcon(QIcon(QPixmap("Icons/Toolbar/format-font-size-less.svg")));
        ui->actionSet_custom_marker_color->setIcon(QIcon(QPixmap("Icons/Toolbar/fill-color.svg")));
        ui->actionNewNotebook->setIcon(QIcon(QPixmap("Icons/Toolbar/document-new.svg")));
        ui->actionNewSection->setIcon(QIcon(QPixmap("Icons/Toolbar/document-new.svg")));
        ui->actionOpen->setIcon(QIcon(QPixmap("Icons/Toolbar/document-open.svg")));
        ui->actionExport_Library->setIcon(QIcon(QPixmap("Icons/Toolbar/document-export.svg")));
        ui->actionExport_Notebook->setIcon(QIcon(QPixmap("Icons/Toolbar/document-export.svg")));
        ui->actionExport_to_HTML->setIcon(QIcon(QPixmap("Icons/Toolbar/document-export.svg")));
        ui->actionSave_all_notebooks_as->setIcon(QIcon(QPixmap("Icons/Toolbar/document-save-all.svg")));
        ui->actionSave_as->setIcon(QIcon(QPixmap("Icons/Toolbar/document-save-as.svg")));
        ui->actionEdit_color->setIcon(QIcon(QPixmap("Icons/Toolbar/fill-color.svg")));
        ui->actionRename->setIcon(QIcon(QPixmap("Icons/Toolbar/document-edit.svg")));
        ui->actionRename_section->setIcon(QIcon(QPixmap("Icons/Toolbar/document-edit.svg")));
        ui->actionQuit->setIcon(QIcon(QPixmap("Icons/Toolbar/window-close.svg")));
    #endif

    actEditor = ui->textEdit;

    actEditor->setFontFamily(this->font().family());
    actEditor->setFontItalic(false);
    actEditor->setFontUnderline(false);
    actEditor->setFontWeight(QFont::Normal);
    actEditor->setFontPointSize(this->font().pointSize()+2);
    actEditor->setAlignment(Qt::AlignLeft);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addNotebookUi(QPushButton *button)
{
    ui->LeftBarButtons->addWidget(button);
}

void MainWindow::setNotebookList(QVector<Section *> *list)
{
    ui->sectionList->clear();
    for(int i=0;i<list->count();i++)
    {
        QListWidgetItem *item = new QListWidgetItem();
        item->setText(list->at(i)->listItem->text());
        item->setData(-1,list->at(i)->listItem->data(-1));
        ui->sectionList->addItem(item);
    }
    ui->sectionList->show();
    ui->hideLeftBar->show();
}

void MainWindow::on_sectionList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    if(current == nullptr) return;
    QString filename = QVariant(current->data(-1)).toString() + current->text();
    loadNotebookSection(filename);
}

void MainWindow::on_textEdit_textChanged()
{
    //save changes
    saveNotebookSection(actFilename);
}

void MainWindow::on_actionBold_triggered()
{
    actEditor->setFontWeight(actEditor->fontWeight() == QFont::Bold ? QFont::Normal : QFont::Bold);
}

void MainWindow::on_actionItalic_triggered()
{
    actEditor->setFontItalic(!actEditor->fontItalic());
}

void MainWindow::on_actionUnderline_triggered()
{
    actEditor->setFontUnderline(!actEditor->fontUnderline());
}

void MainWindow::on_textEdit_cursorPositionChanged()
{
    ui->actionUnderline->setChecked(ui->textEdit->fontUnderline());
    ui->actionBold->setChecked(ui->textEdit->fontWeight() == QFont::Bold ? true  : false);
    ui->actionItalic->setChecked(ui->textEdit->fontItalic());

    ui->actionAlign_to_left->setChecked(ui->textEdit->alignment() == Qt::AlignLeft);
    ui->actionAlign_to_right->setChecked(ui->textEdit->alignment() == Qt::AlignRight);
    ui->actionCenter->setChecked(ui->textEdit->alignment() == Qt::AlignCenter);
    ui->actionJustify->setChecked(ui->textEdit->alignment() == Qt::AlignJustify);

    ui->actionMarker1->setChecked(ui->textEdit->textBackgroundColor() == QColor(100,255,150));
    ui->actionMarker2->setChecked(ui->textEdit->textBackgroundColor() == QColor(255,255,150));
    ui->actionMarker3->setChecked(ui->textEdit->textBackgroundColor() == QColor(255,150,150));

    if(ui->textEdit->textBackgroundColor().alpha() != 0 &&
       ui->textEdit->textBackgroundColor() != QColor(0,0,0) &&
       ui->textEdit->textBackgroundColor() != QColor(100,255,150) &&
       ui->textEdit->textBackgroundColor() != QColor(255,255,150) &&
       ui->textEdit->textBackgroundColor() != QColor(255,150,150))
    {
        customMarkerColor = ui->textEdit->textBackgroundColor();
        //update icon
        QPixmap px(16,16);
        px.fill(QColor(0,0,0,0));
        QPainter painter(&px);
        painter.fillRect(0,0,16,16,customMarkerColor);
        painter.drawText(1,14,"Tt");
        QIcon icon(px);
        ui->actionCustom_marker->setIcon(icon);
        ui->actionCustom_marker->setChecked(true);
    } else {
        ui->actionCustom_marker->setChecked(false);
    }
}

void MainWindow::on_textEdit_copyAvailable(bool b)
{
    ui->actionCopy->setEnabled(b);
}

void MainWindow::on_textEdit_redoAvailable(bool b)
{
    ui->actionRedo->setEnabled(b);
}

void MainWindow::on_textEdit_undoAvailable(bool b)
{
    ui->actionUndo->setEnabled(b);
}

void MainWindow::on_commentsTextEdit_textChanged()
{
    //saveNotebookComments(actFilename);
}

void MainWindow::on_hideLeftBar_clicked()
{
    ui->sectionList->hide();
    ui->hideLeftBar->hide();
}
