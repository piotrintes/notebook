/****************************************************************************
 **
 ** Copyright 2019 Piotr Grosiak     Kenlin.pl Polska
 **
 ** This file is part of Notebook.
 **
 **  Notebook is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Notebook is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QTextEdit>
#include <QFileInfo>
#include "section.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    QString actFilename;                            // Actually opened filename (needed because opened file may or may not be a Notebook setion)
    QColor customMarkerColor = QColor(0,0,0,0);     // Color of currently used custom marker
    QTextEdit *actEditor;                           // Actually active editor (right now used just to shorten code)
public:
    // Ui methods: mainwindow.cpp
    explicit MainWindow(QWidget *parent = nullptr); // Constructior for main window
    ~MainWindow();

    void addNotebookUi(QPushButton *button);        // Add notebook button on left bar
    void setNotebookList(QVector<Section*> *list);  // Set section list for currently selected notebook

    // Functions: mw_functions.cpp
    void loadNotebookSection(QString filename);     // Load file into text editor
    void saveNotebookSection(QString filename);     // Save file opened on text editor
    void openFile(QFileInfo file);                  // Open file using fileinfo object
    void openFile(QString filename);                // Open file using QString as filename
private slots:
    // Methods triggered by ui widgets events: mainwindow.cpp
    void on_sectionList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);
    void on_textEdit_textChanged();
    void on_textEdit_cursorPositionChanged();
    void on_textEdit_copyAvailable(bool b);
    void on_textEdit_redoAvailable(bool b);
    void on_textEdit_undoAvailable(bool b);
    void on_commentsTextEdit_textChanged();
    void on_hideLeftBar_clicked();

    //Methods triggered by toolbar actions: mw_actions.cpp
    void on_actionSelect_all_triggered();
    void on_actionBold_triggered();
    void on_actionItalic_triggered();
    void on_actionUnderline_triggered();
    void on_actionAlign_to_right_triggered();
    void on_actionAlign_to_left_triggered();
    void on_actionCenter_triggered();
    void on_actionJustify_triggered();
    void on_actionUndo_triggered();
    void on_actionRedo_triggered();
    void on_actionCopy_triggered();
    void on_actionCut_triggered();
    void on_actionPaste_triggered();
    void on_actionIncrease_font_size_triggered();
    void on_actionDecrease_font_size_triggered();
    void on_actionMarker1_triggered(bool checked);
    void on_actionMarker2_triggered(bool checked);
    void on_actionMarker3_triggered(bool checked);
    void on_actionCustom_marker_triggered(bool checked);
    void on_actionSet_custom_marker_color_triggered();
    void on_actionOpen_triggered();
    void on_actionQuit_triggered();
    void on_actionNewSection_triggered();
    void on_actionNewNotebook_triggered();
    void on_actionSave_as_triggered();
    void on_actionSave_all_notebooks_as_triggered();
    void on_actionExport_to_HTML_triggered();
    void on_actionStyle_normal_text_triggered();
    void on_actionStyle_Header_1_triggered();
    void on_actionStyle_Header_2_triggered();
    void on_actionStyle_Header_3_triggered();
    void on_actionExport_Notebook_triggered();
    void on_actionExport_Library_triggered();
    void on_action_alpha_triggered();
    void on_action_beta_triggered();
    void on_action_gamma_triggered();
    void on_action_lambda_triggered();
    void on_action_delta_triggered();
    void on_action_pi_triggered();
    void on_action_omegaS_triggered();
    void on_action_epsilon_triggered();
    void on_action_omega_triggered();
    void on_actionRename_section_triggered();
    void on_actionRename_triggered();
    void on_actionEdit_color_triggered();

    void on_actionGreek_toggled(bool arg1);

    void on_actionMathematical_toggled(bool arg1);

    void on_actionComparators_toggled(bool arg1);

    void on_actionArrows_toggled(bool arg1);

    void on_actionSymbols_toggled(bool arg1);

    void on_action_dot_triggered();

    void on_action_minus_triggered();

    void on_action_neq_triggered();

    void on_action_paragraph_triggered();

    void on_action_lessOrEq_triggered();

    void on_action_moreOrEq_triggered();

    void on_action_promil_triggered();

    void on_action_cross_triggered();

    void on_action_div_triggered();

    void on_action_similar_triggered();

    void on_action_plusminus_triggered();

    void on_action_dotOpen_triggered();

    void on_action_inf_triggered();

    void on_action_arrow_lr_triggered();

    void on_action_arrow_l_triggered();

    void on_action_arrow_u_triggered();

    void on_action_arrow_d_triggered();

    void on_action_arrow_r_triggered();

    void on_action_c_triggered();

    void on_action_r_triggered();

    void on_action_euro_triggered();

    void on_action_pe_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
