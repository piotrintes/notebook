/****************************************************************************
 **
 ** Copyright 2019 Piotr Grosiak     Kenlin.pl Polska
 **
 ** This file is part of Notebook.
 **
 **  Notebook is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Notebook is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "notebook.h"
#include <QFile>
#include <QTextStream>
#include <QPainter>
#include <QColorDialog>
#include <QTextBlock>
#include <QTextFormat>
#include <QFileDialog>
#include <QMessageBox>
#include <QInputDialog>


extern QVector<Notebook*> *notebookList;
extern Notebook *actNotebook;
extern QString LibFile;

void MainWindow::loadNotebookSection(QString filename)
{

    /*QString commentsHtml = "";
    QFile filec(filename + ".ntkc");
    if ( filec.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &filec );
        commentsHtml = stream.readAll();
    }

    ui->commentsTextEdit->setHtml(commentsHtml);*/


    actFilename = filename;
    QString sectionHtml = "";
    QFile file(filename + ".ntks");
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        sectionHtml = stream.readAll();
    }
    file.close();
    ui->textEdit->setHtml(sectionHtml);

    ui->actionUndo->setEnabled(false);
    ui->actionRedo->setEnabled(false);
    ui->actionCopy->setEnabled(false);

    if(actNotebook != nullptr)
        ui->statusBar->showMessage("Notebook: " + actNotebook->title + "           "
                               " Section: " + filename.right(filename.length() - actNotebook->path.length()));
    else
        ui->statusBar->showMessage("File: " + filename.right(filename.length()));
}

void MainWindow::saveNotebookSection(QString filename)
{
    QFile file(filename + ".ntks");
    file.remove();
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        stream << ui->textEdit->toHtml();
    }
    file.close();
}


void MainWindow::openFile(QFileInfo file)
{
    if(file.suffix() == "ntk")
    {
        Notebook *notebook = new Notebook(this,file.path() + "/", file.fileName().left(file.fileName().length()-file.suffix().length()-1));
        notebook->select();
    } else if (file.suffix() == "ntkl") {
        QFile filel(file.absoluteFilePath());
        if ( filel.open(QIODevice::ReadWrite) )
        {
            QTextStream stream( &filel );
            while(!stream.atEnd())
            {
                QString line = stream.readLine();
                QString title = line.split("/").last();
                QString path = line.left(line.length() - title.length());
                Notebook *notebook = new Notebook(this,path,title);
                notebook->select();
            }
        }
        filel.close();
        LibFile = file.absoluteFilePath();
    } else if (file.suffix() == "ntks") {
        loadNotebookSection(file.absoluteFilePath().left(file.absoluteFilePath().length()-file.suffix().length()-1));
        on_hideLeftBar_clicked();
    } else {
        if(QMessageBox::question(this,"Open "+file.suffix()+" file",QString(file.suffix().front().toUpper()) +
                                 file.suffix().right(file.suffix().length()-1) + " file can't be opened directly by Notebook.\n"
                                 "Selected file will be converted into Notebook section and saved as "
                                 + file.fileName().left(file.fileName().length()-file.suffix().length()) + "ntks. "
                                 "Oryginal file will not be modyfied.\n"
                                 "Do you wish to proceed?") == QMessageBox::Yes)
        {
            QString newFile = file.absoluteFilePath().left(file.absoluteFilePath().length()-file.suffix().length()-1);
            if (QFile::exists(newFile + ".ntks"))
            {
                if(QMessageBox::question(this,"Open "+file.suffix()+" file","File "
                                         + file.fileName().left(file.fileName().length()-file.suffix().length())
                                         + "ntks alredy exists. Do you want to override this file?") == QMessageBox::No)
                    return;
                QFile::remove(newFile + ".ntks");
            }
            QFile::copy(file.absoluteFilePath(),newFile + ".ntks");

            loadNotebookSection(newFile);
            on_hideLeftBar_clicked();
        }
    }
}

void MainWindow::openFile(QString filename)
{
    QFileInfo file(filename);
    openFile(file);
}
