/****************************************************************************
 **
 ** Copyright 2019 Piotr Grosiak     Kenlin.pl Polska
 **
 ** This file is part of Notebook.
 **
 **  Notebook is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Notebook is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#ifndef NOTEBOOK_H
#define NOTEBOOK_H
#include "orientablepushbutton.h"
#include "section.h"
#include "mainwindow.h"
#include <QString>
#include <QVector>

class Notebook: public QObject
{
private:
    MainWindow *w;                                                  // Pointer to MainWindow
    OrientablePushButton *button = new OrientablePushButton();      // Button representing notebook on ui
    QVector<Section*> *sectionList = new QVector<Section*>;         // List of sections in this notebook

    void loadCommand(QString command, QString argument);            // Interpreter for notebook file entry
    void createIcon();                                              // Create notebook button icon (colored dot)
    void save();                                                    // Save notebook file
public:
    QString path;                                                   // Notebook file path
    QString title;                                                  // Notebook filename
    QColor color;                                                   // Notebook color

    Notebook(MainWindow *w, QString path, QString title);           // Constructor
    void addSection(QString name);                                  // Add notebook section
    void setColor(QColor color);                                    // Set notebook color and recreate icon
    void renameNotebook(QString newName);                           // Rename notebook
    void exportNotebook(QDir dir, QString name, QString suffix);    // Export entire notebook to HTML
    void renameSection(QString actName, QString newName);           // Rename specyfied notebook section
public slots:
    void select();                                                  // Method executed when notebook is selected
};

extern QVector<Notebook*> *notebookList;
extern Notebook *actNotebook;

#endif // NOTEBOOK_H
