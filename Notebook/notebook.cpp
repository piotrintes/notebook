/****************************************************************************
 **
 ** Copyright 2019 Piotr Grosiak     Kenlin.pl Polska
 **
 ** This file is part of Notebook.
 **
 **  Notebook is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Notebook is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with this program.  If not, see <http://www.gnu.org/licenses/>
 **
****************************************************************************/
#include "notebook.h"
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QPainter>

void Notebook::loadCommand(QString command, QString argument)
{
    if(command == "color")
    {
        color = QColor(argument);
        createIcon();
    }
    if(command == "section")
    {
        //add section to notebook
        Section *section = new Section(path, argument);
        sectionList->append(section);
    }
}

void Notebook::createIcon()
{
    //create colored dot
    QPixmap px(16,16);
    px.fill(QColor(0,0,0,0));
    QPainter painter(&px);
    painter.setBrush(color);
    painter.setPen(QPen(color, 4));
    painter.drawEllipse(3,4,8,8);
    QIcon icon(px);
    //set button icon
    button->setIcon(icon);
}

void Notebook::save()
{
    QFile file(path + title + ".ntk");
    file.remove();
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        stream << "color:" + color.name() + "\n";
        for(int i=0;i<sectionList->count();i++)
            stream << "section:" + sectionList->at(i)->title + "\n";
    }
    file.close();
}

void Notebook::setColor(QColor color)
{
    this->color = color;
    createIcon();
    save();
}

void Notebook::renameNotebook(QString newName)
{
    QFile::remove(path + title + ".ntk");
    title = newName;
    button->setText(newName);
    save();
}

void Notebook::exportNotebook(QDir dir, QString name, QString suffix)
{
    QDir d;
    d.mkdir(dir.absolutePath() + "/" + title + "/");

    QFile nbfile("notebook.htm");
    nbfile.copy(dir.absolutePath() + "/" + title + "/notebook.htm");

    for (int i=0;i<sectionList->count();i++) {
        QFile file(sectionList->at(i)->path + "/" + sectionList->at(i)->title + ".ntks");
        file.copy(dir.absolutePath() + "/" + title + "/" + sectionList->at(i)->title + "." + suffix);
    }

    QFile file(dir.absolutePath() + "/" + title + "/list." + suffix);
    file.remove();
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        stream << "<style>\n" <<
                  "body{\n" <<
                  "  background-color: #23c037;\n" <<
                  "  margin-left: 0px;\n" <<
                  "}\n" <<
                  "h3{\n" <<
                  "  color: white;\n" <<
                  "  padding: 10px 0px 10px 20px;\n" <<
                  "}\n" <<
                  "a:link, a:visited {\n" <<
                  "  background-color: #23c037;\n" <<
                  "  color: white;\n" <<
                  "  font-weight: bold;\n" <<
                  "  padding: 10px 0px 10px 20px;\n" <<
                  "  text-decoration: none;\n" <<
                  "  display: inline-block;\n" <<
                  "  width: 90%;\n" <<
                  "  -webkit-transition: color 0.5s, background-color 0.5s;\n" <<
                  "  transition: color 0.5s, background-color 0.5s;\n" <<
                  "}\n" <<
                  "a:hover, a:active, a:focus {\n" <<
                  "  background-color: #188f27;\n" <<
                  "  -webkit-transition: color 0.5s, background-color 0.5s;\n" <<
                  "  transition: color 0.5s, background-color 0.5s;\n" <<
                  "  border: 0" <<
                  "}\n" <<
                  "</style>\n" <<
                  "<h3>" + title + "</h3>\n";
        for(int i=0;i<sectionList->count();i++)
        {
            QString sectionFile = sectionList->at(i)->title + "." + suffix;
            stream << "<a href=\"" << sectionFile << "\" target=main>" << sectionList->at(i)->title << "</a><br>"  ;
        }
    }
    file.close();

    QFile indexFile(dir.absolutePath() + "/" + name);
    indexFile.remove();
    if ( indexFile.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &indexFile );
        stream << "<frameset cols=\"200,*\" border=0>";
        stream << "<frame src=\"" + title + "/list." + suffix + "\">";
        stream << "<frame name=main src=\"" + title + "/notebook.htm\">";
        stream << "</frameset>";
    }
    indexFile.close();
}

void Notebook::renameSection(QString actName, QString newName)
{
    for (int i=0;i<sectionList->count();i++)
        if (sectionList->at(i)->title == actName)
        {
            sectionList->at(i)->title = newName;
            sectionList->at(i)->listItem->setText(newName);
            save();
        }
}

void Notebook::addSection(QString name)
{
    Section *section = new Section(path, name);
    sectionList->append(section);
    save();
    w->setNotebookList(sectionList);
}

void Notebook::select()
{
    w->setNotebookList(sectionList);
    button->setChecked(true);
    actNotebook = this;
}

Notebook::Notebook(MainWindow *w, QString path, QString title)
{
    this->w = w;
    this->path = path;
    this->title = title;
    notebookList->append(this);

    //create button
    button->setText(title);
    button->setMaximumWidth(35);
    button->setFlat(true);
    button->setAutoExclusive(true);
    button->setCheckable(true);
    w->addNotebookUi(button);
    button->setOrientation(OrientablePushButton::VerticalBottomToTop);
    connect(button, &OrientablePushButton::clicked,this, &Notebook::select);

    //load notebook file
    QFile file(path + title + ".ntk");
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        while(!stream.atEnd())
        {
            QString line = stream.readLine();
            loadCommand(line.split(":").at(0), line.split(":").at(1));
        }
    }
    file.close();
}
